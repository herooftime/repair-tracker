# repair-tracker

This program is intented to help track vehicle repairs and issues. Ultimately,
it will serve as a tool to make tracking vehicle repairs and issues easier.

## Contact

mr.carpinelli@protonmail.ch

## Contributing

This is mostly a personal project, but any contributions are welcome and
appreciated.

## Building

This is just a simple C++ console application.

## License

All contributions are made under the [GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html). See the [LICENSE](LICENSE).
