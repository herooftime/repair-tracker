// Turn getStringList into a bool function that does not call exit(1)

// Write Delete Function, but include ability to delete up to next line
/* Delete string from file
string deleteline;
string line;

ifstream fin;
fin.open("example.txt");
ofstream temp;
temp.open("temp.txt");
cout << "Which line do you want to remove? ";
cin >> deleteline;




while (getline(fin,line))
{
  line.replace(line.find(deleteline),deleteline.length(),"");
  temp << line << endl;
}

temp.close();
fin.close();
remove("example.txt");
rename("temp.txt","example.txt");
*/

#include "io.hpp"

#include <iostream>
#include <fstream>

namespace io
{
  std::string getPath(char *in_argv[])
  {
    const std::string PROGRAM_NAME = "repair-tracker";
    std::string path;

    path = in_argv[0];
    path = path.substr(0, (path.length() - PROGRAM_NAME.length()));

    return path;
  }

  bool writeToFile(std::string in_data, std::string in_file_path)
  {
    std::ifstream output_file;

    output_file.open(in_file_path);
    if (output_file.fail())
      {
        std::cerr << "There was an error opening the file. File write failed.\n\n";

        return false;
      }

    getline(output_file, in_data);
    if (output_file.fail())
      {
        std::cerr << "There was an error opening the file. File write failed.\n\n";

        return false;
      }

    else // (out_file.fail() == false)
      {
        return true;
      }

  }

  bool writeToFile(std::string in_data, std::ifstream in_file)
  {
    getline(out_file, in_data);
    if (out_file.fail())
      {
        std::cerr << "There was an error opening the file. File write failed.\n\n";

        return false;
      }

    else // (out_file.fail() == false)
      {
        return true;
      }
  }

  std::vector<std::string> getStringList(std::string in_file_location)
  {
    std::string text;
    std::vector<std::string> string_list;
    std::ifstream list_file;

    list_file.open(in_file_location);
    if (list_file.fail())
      {
        std::cerr << "Error opening file.\n\n"
                  << "Exiting program...\n\n";
        exit(1);
      }

    while (getline(list_file, text))
      {
        string_list.push_back(text);
      }

    list_file.close();

    return string_list;
  }

  std::vector<std::string> getVehicleMenu(char *in_argv[])
  {
    std::string path;

    path = getPath(in_argv);
    path = path + "data/vehicle_list.dat";

    return getStringList(path);
  }

  void printSeparator()
  {
    for (int i = 0; i < 80; i++)
      std::cout << "=";

    std::cout << "\n\n";
  }

  void printWrapped(std::string in_string)
  {
          for (int i = 0; i < 80; i++)
          {
                  std::cout << "=";
          }

          std::cout << std::endl;

          for (unsigned int i = 0; i < (40 - (in_string.length() / 2)); i++)
          {
                  std::cout << " ";
          }

          std::cout << in_string;

          for (unsigned int i = 0; i < (40 - (in_string.length() / 2)); i++)
          {
                  // Increment i for odd length text (one less space)
                  if (in_string.length() % 2 != 0)
                          i++;

                  std::cout << " ";
          }

          std::cout << std::endl;

          for (int i = 0; i < 80; i++)
          {
                  std::cout << "=";
          }

          std::cout << "\n";
  }

  void printVehicleMenu(char *in_argv[])
  {
    int option;
    std::vector<std::string> vehicle_list;

    vehicle_list = getVehicleMenu(in_argv);

    printWrapped("Vehicles");

    for (unsigned int i = 0; i < vehicle_list.size(); i++)
      {
        std::cout << "\t" << (i +1) << ". " << vehicle_list[i] << "\n"; // enum containing alphabet chars using i
      }

    std::cout << "\t" << (vehicle_list.size() + 1) << ". Add/Remove Vehicles\n"
              << "\t" << (vehicle_list.size() + 2) << ". Exit Program\n\n";

    std::cout << "Please select a vehicle: ";
    std::cin >> option;
    std::cout << "\n";

    if ((static_cast<unsigned int>(option) <= vehicle_list.size()) && (option > 0))
      {
        std::cout << "WRITING TO FILE ACTIVE_VEHICLE:\n\n"; // HERE HERE HERE HERE HERE HERE HERE HERE HERE HERE HERE HERE HERE HERE HERE HERE HERE HERE
        // writeToFile(vehicle_list[option - 1], "active_vehicle:");
        printMainMenu();
      }

    else if (static_cast<unsigned int>(option) == (vehicle_list.size() + 1))
      {
        printAlterVehicleListMenu(in_argv);
      }

    else if (static_cast<unsigned int>(option) == (vehicle_list.size() + 2))
      {
        std::cout << "Exiting program...\n\n";
      }

    else
      {
        std::cerr << "\nError, invalid option.\n\n";
        printVehicleMenu(in_argv);
      }
  }

  void printMainMenu()
  {
    int option;

    printWrapped("Main Menu");

    std::cout << "\t 1. History & Records\n"
              << "\t 2. Add/Update Records\n"
              << "\t 3. Exit Program\n"
              << "\nPlease select one of the above options: ";
    std::cin >> option;
    std::cout << "\n";

    switch (option)
      {
      case 1:
        std::cout << "printRecordsMenu()\n\n";
        break;

      case 2:
        std::cout << "printUpdateMenu()\n\n";
        break;

      case 3:
        std::cout << "Exiting program...\n\n";
        break;

      default:
        std::cout << "Invalid input. Please try again.\n\n";
      }
  }
  void printAlterVehicleListMenu(char *in_argv[])
  {
    (void)in_argv;
    std::cout << "printAlterVehiclesListMenu(char *in_argv)\n\n";
  }
}

/*
void printRecordsMenu()
{
    char menu_input;

    std::cout << "\n\tHistory & Records:\n"
              << "\tA. Work Done\n"
              << "\tB. Work Needed\n"
              << "\tC. Works In-Progress\n"
              << "\tQ. Exit\n"
              << "\nPlease select one of the above options: ";
*/
