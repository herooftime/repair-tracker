#ifndef IO_HPP
#define IO_HPP


#include <string>
#include <vector>

namespace io
{
  std::string getPath(char *in_argv[]);
  // Returns path of executable without program name using argv[]

  bool writeToFile(std::string in_data, std::string in_file_path);
  // Writes string to file, eturns true or false based on success

  bool writeToFile(std::string in_data, std::ifstream in_file);
  // Writes string to file, eturns true or false based on success

  std::vector<std::string> getStringList(std::string in_file_location);
  // Returns a vector containing a list of vehicles added to the program

  void printWrapped(std::string in_string);
  // Prints an '=' frame wrapped around string arguement like a title

  std::vector<std::string> getVehicleMenu(char *in_argv[]);
  // Returns a vector containing vehicle menu, takes argv[] for getPath(char *in_argv[])

  void printSeparator();
  // Prints 80 '=' characters

  void printMainMenu();
  // Prints main menu, calls subsequent menus when necessary

  void printVehicleMenu(char *in_argv[]);
  // Prints vehicle menu, calls subsequent output functions

  void printAlterVehicleListMenu(char *in_argv[]);
  // Prints alter vehicle list menu, calls subsequent output functions
}


#endif // IO_HPP
