// Write path to state_file
// to state_file user option for vehicle (config/state file)

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "io.hpp"

int main(int argc, char *argv[])
{
  std::cout << "\nWelcome to repair-tracker\n\n";

  io::printVehicleMenu(argv);

  (void)argc;

  return 0;
}
